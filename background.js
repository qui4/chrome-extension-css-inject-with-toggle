const STORAGE_KEY = 'dark-mode';

async function _getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab;
}

function _getDarkMode(tab) {
    const key = `${STORAGE_KEY}_${tab.id}`;
    return new Promise(resolve => chrome.storage.local.get([key], result => resolve(result[key])))
}

async function _applyDarkMode(toggle = false) {
    const tab = await _getCurrentTab();
    const currentValue = await _getDarkMode(tab);
    const value = toggle ? !currentValue : currentValue;
    if (tab.url.startsWith('http')) {
        /**
         * Execute a piece of javascript code in the main thread of one of the tabs
         */
        chrome.scripting.executeScript({
            target: { tabId: tab.id },
            func: value
                ? () => document.body.classList.add('dark-mode')
                : () => document.body.classList.remove('dark-mode'),
        });

        /**
         * Set the extension icon
         */
        chrome.action.setIcon({ path: value ? 'moon.png' : 'sun.png' });

        /**
         * Save the new "darkMode" state in the chrome.storage in case it was toggled
         *
         * NOTE: chrome.storage can only be used when the "storage" permissions are set in manifest.json
         */
        if (toggle) {
            chrome.storage.local.set({ [`${STORAGE_KEY}_${tab.id}`]: value });
        }
    }
}

async function onTabUpdate(tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete') {
        /**
         * This check on http* is added to prevent errors when executing this code on other page types.
         * The chrome.scripting API does not work on (e.g.) chrome://* pages.
         */
        if (tab.url.startsWith('http')) {
            /**
             * NOTE: chrome.scripting can only be used when the "scripting" permissions are set in manifest.json
             */
            chrome.scripting.insertCSS({
                target: { tabId },
                files: ['default.css']
            });
        }
    }

    await _applyDarkMode();
}

async function onActiveTabChanged() {
    await _applyDarkMode();
}

async function onExtensionIconClick() {
    await _applyDarkMode(true);
}

/**
 * Listen to changes in any tabs (e.g. start-loading, loading-complete, change in url, etc.)
 *
 * NOTE: chrome.tabs can only be used when the "tabs" permissions are set in manifest.json
 */
chrome.tabs.onUpdated.addListener(onTabUpdate);

/**
 * Listen to when another tab is activated
 */
chrome.tabs.onActivated.addListener(onActiveTabChanged);

/**
 * Listen to click events on the extension icon
 *
 * NOTE: chrome.action can only be used if the "action" object is defined in manifest.json
 */
chrome.action.onClicked.addListener(onExtensionIconClick);
